#!/usr/bin/env python
from ROOT import *

from array import array
import sys,math

nColors=101
r = [1.00, 0.00, 0.00, 0.70, 1.00]
g = [1.00, 0.70, 0.60, 0.50, 0.00]
b = [1.00, 1.00, 0.25, 0.00, 0.00]
s = [0.00, 0.25, 0.50, 0.75, 1.00]
ss = array('d',s)
rr = array('d',r)
gg = array('d',g)
bb = array('d',b)
TColor.CreateGradientColorTable(len(ss), ss, rr, gg, bb, nColors)
gStyle.SetNumberContours(nColors)
pi=3.14159265

rand=TRandom()

if __name__=='__main__':
  beta = 21.6*pi/180.
  gamma = 69.3*pi/180.
  piMinusAlphaPlusBeta = (1-(21.6+89.4)/180)*pi
  rho= 0.132
  eta=0.344




  hist = TH2F("hist","",300,-0.1,0.5,300,-0.05,0.55)
  betahist = hist.Clone("betahist")

  n=int(100*1000*1000)
  for i in range(1,n):
    frac=10*float(i)/n;
    if frac == int(frac):
      print str(10*int(frac))+"% done."
    r = sqrt(TMath.Abs(rand.Gaus(0,0.5)))
    phi = 0;
    if(rand.Rndm()>0.5):
      phi = TMath.Abs(rand.Gaus(0,9.4))
    else:
      phi =-TMath.Abs(rand.Gaus(0,8.8))
    phi=phi+69.3
    hist.Fill(r*cos(phi*pi/180.),r*sin(phi*pi/180.));

    #r = sqrt(TMath.Abs(rand.Gaus(0,0.5)))
    #phi = rand.Gaus(pi-beta,0.045)
    #betahist.Fill(1+r*cos(phi),r*sin(phi));



  canvas = TCanvas("prettyTriangle","",800,800)
  canvas.SetTopMargin(0)
  canvas.SetLeftMargin(0)
  canvas.SetRightMargin(0)
  canvas.SetBottomMargin(0)
  betahist.GetXaxis().SetNdivisions(0);
  betahist.GetYaxis().SetNdivisions(0);
  hist.GetXaxis().SetNdivisions(0);
  hist.GetYaxis().SetNdivisions(0);
  betahist.SetStats(0)
  hist.SetStats(0)


  hist.Draw("COL")
  

  l1 = TLine(0,0,1,0)
  l1.SetLineWidth(7)
  l1.Draw()
  l2 = TLine(1,0,1+cos(pi-beta),sin(pi-beta))
  l2.SetLineWidth(7)
  l2.Draw()

  m1 = TMarker(rho,eta,20)
  m1.SetMarkerSize(8)
  m1.Draw()

  r=0.45
  l4 = TLine(0,0,r*cos(68*pi/180),r*sin(68*pi/180))
  l4.SetLineWidth(7)
  l4.SetLineStyle(2);
  l4.Draw()
  r=0.5
  l5 = TLine(0,0,r*cos(69*pi/180),r*sin(69*pi/180))
  l5.SetLineWidth(7)
  l5.SetLineStyle(7);
  l5.Draw()
  r=0.5
  l6 = TLine(0,0,r*cos(71.1*pi/180),r*sin(71.1*pi/180))
  l6.SetLineWidth(7)
  l6.SetLineStyle(4);
  l6.Draw()

  t0 = TPaveLabel(-0.05,-0.05,0,0,"0")
  t0.SetFillStyle(0)
  t0.SetFillColor(0)
  t0.SetBorderSize(0)
  t0.SetTextFont(132)
  t0.Draw()

  m2 = TMarker(0,0,20)
  m2.SetMarkerSize(3)
  m2.Draw()

  t1 = TPaveLabel(0.20,0.13,0.25,0.18,"#gamma")
  t1.SetFillStyle(0)
  t1.SetFillColor(0)
  t1.SetBorderSize(0)
  t1.SetTextFont(132)
  t1.SetTextSize(2)
  t1.Draw()

  arc = TArc()
  arc.SetFillStyle(0)
  arc.SetLineWidth(7)
  arc.SetLineColor(1)
  arc.DrawArc(0,0,0.2,4,63,"only |>")

  t2 = TPaveLabel(0.17,0.37,0.32,0.42,"Belle")
  t2.SetFillStyle(1001)
  t2.SetFillColor(0)
  t2.SetBorderSize(1)
  t2.SetTextFont(132)
  t2.SetLineWidth(7)
  t2.SetLineStyle(2);
  t2.Draw()

  t3 = TPaveLabel(0.18,0.44,0.33,0.49,"BaBar")
  t3.SetFillStyle(1001)
  t3.SetFillColor(0)
  t3.SetBorderSize(1)
  t3.SetTextFont(132)
  t3.SetLineWidth(7)
  t3.SetLineStyle(7);
  t3.Draw()

  t4 = TPaveLabel(0.02,0.48,0.17,0.53,"LHCb")
  t4.SetFillStyle(1001)
  t4.SetFillColor(0)
  t4.SetBorderSize(1)
  t4.SetTextFont(132)
  t4.SetLineWidth(7)
  t4.SetLineStyle(4);
  t4.Draw()

  raw_input("Press <return> to quit.")
  canvas.Print(canvas.GetName()+".pdf") ;


