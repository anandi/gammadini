#!/usr/bin/env python
import os,time

tot=100

def submitGeneration(parameters,config):
  pwd=os.getenv("PWD")
  exe="bin/gammadini"
  for i in range(0,tot):
    for par in parameters:
      opt=" -p "+par+" -n 100 "+config+" -l toydata -E "
      print(str(i+1)+"/"+str(tot)+") "+exe+opt)
      time.sleep(0.25)
      t=time.time()
      # BSUB AT CERN
      os.system("export exe="+exe+"; export dir="+pwd+"; export opt='"+opt+"'; bsub -q 8nh -o $HOME/SCRATCH0/out_"+str(i)+"_"+str(t)+".out "+pwd+"/aux/mjohn/submitLSF.csh")

def submitFull(config):
  parameters=[]
  parameters.append("gamma")
  parameters.append("rBu_dk")
  parameters.append("rBu_dpi")
  parameters.append("deltaBu_dk")
  parameters.append("deltaBu_dpi")
  submitGeneration(parameters,config)

def submitDKstarStudy(config):
  parameters=[]
  parameters.append("rBd_dkstar")
  parameters.append("deltaBd_dkstar")
  submitGeneration(parameters,config)

def submitDKONLY(config):
  config=config+" -a DKONLY"
  parameters=[]
  parameters.append("gamma")
  parameters.append("rBu_dk")
  parameters.append("deltaBu_dk")
  submitGeneration(parameters,config)

def submitDPIONLY(config):
  config=config+" -a DPIONLY"
  parameters=[]
  parameters.append("gamma")
  parameters.append("rBu_dpi")
  parameters.append("deltaBu_dpi")
  submitGeneration(parameters,config)

if __name__=='__main__':
  if 1:
    submitFull("-r 1,3,5,6")    #K3pi+CLEO only
    submitDKONLY("-r 1,3,5,6")  #K3pi+CLEO only
    submitDPIONLY("-r 1,3,5,6") #K3pi+CLEO only
  if 0:
    submitDKstarStudy(" -a DKONLY ")
  if 0:
    submitFull("-r 6")#removes DK*
    submitDKONLY("-r 6")#removes DK*
    submitDPIONLY("-r 3,6")#removes GGSZ, DK*
