#! /usr/bin/env python

from ROOT import *
import array,numpy,math

def printCorr(filename,varnames):
  f= TFile(filename,"READ")
  f.cd()

  var=[]
  err=[]
  arglist=RooArgList()
  for name in varnames:
    v=fitresult_model_reducedData_binned.floatParsFinal().find(name).getVal()
    e=fitresult_model_reducedData_binned.floatParsFinal().find(name).getError()
    print "  observedCentralValues(Index[\""+name+"\"]) = {0:.7f};".format(v)
    rrv=RooRealVar(name,"",-1,1)
    arglist.add(rrv)
    var.append(rrv)
    err.append(e)

  cov=fitresult_model_reducedData_binned.reducedCovarianceMatrix(arglist)
  cor=cov.Clone()
  cor.Zero()

  print "  TVectorD statErrorValues(_nmeasurements);"
  for i in range(len(varnames)):
    print "  statErrorValues(Index[\""+varnames[i]+"\"]) = {0:.7f};".format(err[i])
    for j in range(len(varnames)):
      ar=numpy.array([cov(i,j)/(err[i]*err[j])])
      cor.InsertRow(i,j,ar,1)

  print '  const double statCorrelations['+str(int(math.pow(len(varnames),2)))+'] = {',
  for i in range(len(varnames)):
    for j in range(len(varnames)):
      if 0==j:
        print "\n    ",
      if(cor(i,j)>=0): print "",
      print "{0:.2f},".format(cor(i,j)),
      if i==len(varnames)-1 and j==len(varnames)-1:
        print "\b\b};    "
  print "  TMatrixDSym statCorr(_nmeasurements,statCorrelations);\n"

def kthreepi():
  #filename="input/lhcbRunOne/sept2012K3PIResult.root"
  filename="input/lhcbRunOne/july2013K3PIResult.root"

  names=[]
  names.append("A_FAV_b2dk_d2kpipipi")
  names.append("R_minus_b2dk_d2pikpipi")
  names.append("R_plus_b2dk_d2pikpipi")
  printCorr(filename,names)

  names=[]
  names.append("A_FAV_b2dpi_d2kpipipi")
  names.append("R_minus_b2dpi_d2pikpipi")
  names.append("R_plus_b2dpi_d2pikpipi")
  printCorr(filename,names)

  names=[]
  names.append("A_FAV_b2dk_d2kpipipi")
  names.append("A_FAV_b2dpi_d2kpipipi")
  names.append("R_dk_vs_dpi_d2kpipipi")
  names.append("R_minus_b2dk_d2pikpipi")
  names.append("R_minus_b2dpi_d2pikpipi")
  names.append("R_plus_b2dk_d2pikpipi")
  names.append("R_plus_b2dpi_d2pikpipi")
  printCorr(filename,names)

def twobody():
  filename="input/lhcbRunOne/winter2012ADSGLWResult.root"
  filename="input/lhcbRunOne/july2013ADSGLWResult.root"

  names=[]
  names.append("A_CPP_b2dk_d2kk")
  names.append("A_CPP_b2dk_d2pipi")
  names.append("A_FAV_b2dk_d2kpi")
  names.append("R_minus_b2dk_d2pik")
  names.append("R_plus_b2dk_d2pik")
  printCorr(filename,names)
  
  names=[]
  names.append("A_CPP_b2dpi_d2kk")
  names.append("A_CPP_b2dpi_d2pipi")
  names.append("A_FAV_b2dpi_d2kpi")
  names.append("R_minus_b2dpi_d2pik")
  names.append("R_plus_b2dpi_d2pik")
  printCorr(filename,names)
  
  names=[]
  names.append("A_CPP_b2dk_d2kk")
  names.append("A_CPP_b2dk_d2pipi")
  names.append("A_CPP_b2dpi_d2kk")
  names.append("A_CPP_b2dpi_d2pipi")
  names.append("A_FAV_b2dk_d2kpi")
  names.append("A_FAV_b2dpi_d2kpi")
  names.append("R_dk_vs_dpi_d2kk")
  names.append("R_dk_vs_dpi_d2kpi")
  names.append("R_dk_vs_dpi_d2pipi")
  names.append("R_minus_b2dk_d2pik")
  names.append("R_minus_b2dpi_d2pik")
  names.append("R_plus_b2dk_d2pik")
  names.append("R_plus_b2dpi_d2pik")
  printCorr(filename,names)

#kthreepi()
twobody()