
#------------------------------------------------ DEFINITIONS ---------------------------------------------

ROOTCFLAGS   := $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS     := $(shell $(ROOTSYS)/bin/root-config --libs)

CXXFLAGS      = -O -Wall -fPIC -g -W
CXXFLAGS     += $(ROOTCFLAGS) 

LIBS	      = $(ROOTLIBS) -lTreePlayer -lTMVA -lRooFitCore -lRooFit -lMinuit -lFoam -lXMLIO

CC            = c++
EXECUTABLE    = gammadini
BINDIR        = ${PWD}/bin/
SRCDIR        = ${PWD}/src/

#------------------------------------- OBJECTS TO INCLUDE IN COMPILATION ----------------------------------

OBJS	      = $(BINDIR)Main.o $(BINDIR)Base.o $(BINDIR)CommonTools.o $(BINDIR)Parameters.o 
OBJS         += $(BINDIR)Frequentist.o $(BINDIR)Method.o $(BINDIR)Scan.o $(BINDIR)GenToys.o $(BINDIR)Coverage.o 
OBJS         += $(BINDIR)Pot.o $(BINDIR)Result.o $(BINDIR)FitParameter.o $(BINDIR)ADSGLWBase.o
OBJS         += $(BINDIR)Bu2D0H_D02HH.o $(BINDIR)Bu2D0H_D02KSHH.o $(BINDIR)Bu2D0H_D02KSKPI.o $(BINDIR)Bu2D0H_D02KPIPIPI.o $(BINDIR)Bu2D0H_D02KPIPIPI_BINNED.o 
OBJS         += $(BINDIR)Bu2D0H_D02KPIPI0.o $(BINDIR)Bd2D0KPI_D02HH.o $(BINDIR)Bd2D0KPI_D02KSHH.o $(BINDIR)Bs2DsK.o $(BINDIR)Bu2Dstar0H_D02HH.o 
OBJS         += $(BINDIR)Bu2D0HHH_D02HH.o $(BINDIR)Bu2D0HHH_D02KSHH.o
OBJS         += $(BINDIR)Bu2D0Kstar_D02HH.o
OBJS         += $(BINDIR)DMIXING.o $(BINDIR)CLEO_FCP.o $(BINDIR)CLEO_COH.o
OBJS         += $(BINDIR)LHCb_COH.o
OBJS         += $(BINDIR)CARTESIAN.o

#------------------------------------------------ EXECUTABLE ----------------------------------------------

$(EXECUTABLE) :	$(OBJS)
				${CC} $(LIBS) -o $(BINDIR)$(EXECUTABLE) $(OBJS) 

#---------------------------------------------- CORE CLASSES ----------------------------------------------

$(BINDIR)Main.o : $(SRCDIR)Main.C
				@mkdir -p $(BINDIR)
				@mkdir -p output
				${CC} $(CXXFLAGS) -c $(SRCDIR)Main.C -o $(BINDIR)Main.o

$(BINDIR)Base.o : $(SRCDIR)Base.C $(SRCDIR)Base.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Base.C -o $(BINDIR)Base.o

$(BINDIR)CommonTools.o : $(SRCDIR)CommonTools.C $(SRCDIR)CommonTools.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)CommonTools.C -o $(BINDIR)CommonTools.o

$(BINDIR)Parameters.o : $(SRCDIR)Parameters.C $(SRCDIR)Parameters.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Parameters.C -o $(BINDIR)Parameters.o

$(BINDIR)Frequentist.o : $(SRCDIR)Frequentist.C $(SRCDIR)Frequentist.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Frequentist.C -o $(BINDIR)Frequentist.o

$(BINDIR)Method.o : $(SRCDIR)Method.C $(SRCDIR)Method.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Method.C -o $(BINDIR)Method.o

$(BINDIR)Scan.o : $(SRCDIR)Scan.C $(SRCDIR)Scan.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Scan.C -o $(BINDIR)Scan.o

$(BINDIR)GenToys.o : $(SRCDIR)GenToys.C $(SRCDIR)GenToys.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)GenToys.C -o $(BINDIR)GenToys.o

$(BINDIR)Coverage.o : $(SRCDIR)Coverage.C $(SRCDIR)Coverage.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Coverage.C -o $(BINDIR)Coverage.o

$(BINDIR)Pot.o : $(SRCDIR)Pot.C $(SRCDIR)Pot.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Pot.C -o $(BINDIR)Pot.o

$(BINDIR)Result.o : $(SRCDIR)Result.C $(SRCDIR)Result.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Result.C -o $(BINDIR)Result.o

$(BINDIR)FitParameter.o : $(SRCDIR)FitParameter.C $(SRCDIR)FitParameter.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)FitParameter.C -o $(BINDIR)FitParameter.o

$(BINDIR)ADSGLWBase.o :   $(SRCDIR)ADSGLWBase.C $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)ADSGLWBase.C -o $(BINDIR)ADSGLWBase.o

#--------------------------------------------- RESULT CLASSES ---------------------------------------------

$(BINDIR)Bu2D0H_D02HH.o : $(SRCDIR)Bu2D0H_D02HH.C $(SRCDIR)Bu2D0H_D02HH.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02HH.C -o $(BINDIR)Bu2D0H_D02HH.o

$(BINDIR)Bu2D0H_D02KSHH.o : $(SRCDIR)Bu2D0H_D02KSHH.C $(SRCDIR)Bu2D0H_D02KSHH.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02KSHH.C -o $(BINDIR)Bu2D0H_D02KSHH.o

$(BINDIR)Bu2D0H_D02KSKPI.o : $(SRCDIR)Bu2D0H_D02KSKPI.C $(SRCDIR)Bu2D0H_D02KSKPI.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02KSKPI.C -o $(BINDIR)Bu2D0H_D02KSKPI.o

$(BINDIR)Bu2D0H_D02KPIPIPI.o : $(SRCDIR)Bu2D0H_D02KPIPIPI.C $(SRCDIR)Bu2D0H_D02KPIPIPI.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02KPIPIPI.C -o $(BINDIR)Bu2D0H_D02KPIPIPI.o

$(BINDIR)Bu2D0H_D02KPIPIPI_BINNED.o : $(SRCDIR)Bu2D0H_D02KPIPIPI_BINNED.C $(SRCDIR)Bu2D0H_D02KPIPIPI_BINNED.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02KPIPIPI_BINNED.C -o $(BINDIR)Bu2D0H_D02KPIPIPI_BINNED.o

$(BINDIR)Bd2D0KPI_D02HH.o : $(SRCDIR)Bd2D0KPI_D02HH.C $(SRCDIR)Bd2D0KPI_D02HH.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bd2D0KPI_D02HH.C -o $(BINDIR)Bd2D0KPI_D02HH.o

$(BINDIR)Bd2D0KPI_D02KSHH.o : $(SRCDIR)Bd2D0KPI_D02KSHH.C $(SRCDIR)Bd2D0KPI_D02KSHH.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bd2D0KPI_D02KSHH.C -o $(BINDIR)Bd2D0KPI_D02KSHH.o

$(BINDIR)Bu2Dstar0H_D02HH.o : $(SRCDIR)Bu2Dstar0H_D02HH.C $(SRCDIR)Bu2Dstar0H_D02HH.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2Dstar0H_D02HH.C -o $(BINDIR)Bu2Dstar0H_D02HH.o

$(BINDIR)Bs2DsK.o : $(SRCDIR)Bs2DsK.C $(SRCDIR)Bs2DsK.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bs2DsK.C -o $(BINDIR)Bs2DsK.o

$(BINDIR)Bu2D0HHH_D02HH.o : $(SRCDIR)Bu2D0HHH_D02HH.C $(SRCDIR)Bu2D0HHH_D02HH.h  $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0HHH_D02HH.C -o $(BINDIR)Bu2D0HHH_D02HH.o

$(BINDIR)Bu2D0HHH_D02KSHH.o : $(SRCDIR)Bu2D0HHH_D02KSHH.C $(SRCDIR)Bu2D0HHH_D02KSHH.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0HHH_D02KSHH.C -o $(BINDIR)Bu2D0HHH_D02KSHH.o

$(BINDIR)Bu2D0H_D02KPIPI0.o : $(SRCDIR)Bu2D0H_D02KPIPI0.C $(SRCDIR)Bu2D0H_D02KPIPI0.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0H_D02KPIPI0.C -o $(BINDIR)Bu2D0H_D02KPIPI0.o	

$(BINDIR)Bu2D0Kstar_D02HH.o : $(SRCDIR)Bu2D0Kstar_D02HH.C $(SRCDIR)Bu2D0Kstar_D02HH.h $(SRCDIR)ADSGLWBase.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)Bu2D0Kstar_D02HH.C -o $(BINDIR)Bu2D0Kstar_D02HH.o

$(BINDIR)DMIXING.o : $(SRCDIR)DMIXING.C $(SRCDIR)DMIXING.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)DMIXING.C -o $(BINDIR)DMIXING.o

$(BINDIR)CLEO_COH.o : $(SRCDIR)CLEO_COH.C $(SRCDIR)CLEO_COH.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)CLEO_COH.C -o $(BINDIR)CLEO_COH.o

$(BINDIR)CLEO_FCP.o :         $(SRCDIR)CLEO_FCP.C $(SRCDIR)CLEO_FCP.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)CLEO_FCP.C -o $(BINDIR)CLEO_FCP.o

$(BINDIR)LHCb_COH.o : $(SRCDIR)LHCb_COH.C $(SRCDIR)LHCb_COH.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)LHCb_COH.C -o $(BINDIR)LHCb_COH.o

$(BINDIR)CARTESIAN.o : $(SRCDIR)CARTESIAN.C $(SRCDIR)CARTESIAN.h
				${CC} $(CXXFLAGS) -c $(SRCDIR)CARTESIAN.C -o $(BINDIR)CARTESIAN.o

#------------------------------------------------- CLEAN -------------------------------------------------
clean:
		@mkdir -p $(BINDIR)
		@rm -f $(BINDIR)$(EXECUTABLE) $(SRCDIR)/*~ core* $(BINDIR)/*.o ./*~
		@rmdir $(BINDIR)
# DO NOT DELETE
