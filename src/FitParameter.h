//-----------------------------------------------------------------------------
// Declaration of class : FitParameter
//
// Purpose : Holds information about an individual parameter. This includes
//           names, limits and index number used by Minuit.
//
// Author : Malcolm John, 2012-08-18
//-----------------------------------------------------------------------------
#ifndef _FitParameter_h
#define _FitParameter_h

#include <map>
#include <string>
#include <vector>

#include "TH1F.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TVirtualPad.h"

#include "Base.h"
#include "Parameters.h"


using namespace std;

class FitParameter :public Base {
  public:
  FitParameter(string,double,double,double,double,double,double,string,string="");
  ~FitParameter(){}
  void passParameters(Parameters* p){jobpar=p;}
  int number(){return _num;}
  void number(int n){ _num=n;}
  string name(){ return _name;}
  string title(){ return _title;}
  string latex(string);
  string unit(){ return _unit;}
  bool isAnAngle(){ return _isAnAngle;}
  void value(double v){_value=v;}
  double value(){return _value;}
  void initialVal(double ini){_initial=ini;}
  double initialVal(){return _initial;}
  double stepSize(){return _stepSize;}
  double loFitLim(){return _lofitlim;}
  double hiFitLim(){return _hifitlim;}
  double fitRange(){return _hifitlim-_lofitlim;}
  double loPlotLim(){return _loplotlim;}
  double hiPlotLim(){return _hiplotlim;}
  double plotRange(){return _hiplotlim-_loplotlim;}
  void valueAtGlobalMin(double v){_valueAtGlobalMin=v;}
  double valueAtGlobalMin(){return _valueAtGlobalMin;}
  void fill(double,double,double);
  void draw(string,string,bool);
  void coverageSummary();
  void setStyle(TVirtualPad*,TH1*,string,string,string,string="");
private:
  Parameters* jobpar;
  int _num;
  string _name;
  string _title;
  string _unit;
  double _value;
  double _initial;
  double _stepSize;
  double _lofitlim;
  double _loplotlim;
  double _hifitlim;
  double _hiplotlim;
  double _valueAtGlobalMin;
  bool _isAnAngle;

  vector<float> dChi2Values;
  vector<float> probValues;
  vector<float> scanValues;

  vector<TGraph*> graphs;
  vector<TH1F*> histograms;
  vector<TH1F*> Ghistograms;
  map<float,TH2F*> twoDhistograms;
  map<float,vector<float> > deltaChi2VsPoi;
};

#endif
