//-----------------------------------------------------------------------------
// Declaration of class : Result
//
// Purpose : Base class for all the contributing results (LHCb or charm-system)
//           Importantly the virtual methods will generally be overloaded in
//           the derived classes.
//
// Author : Malcolm John, 2012-08-14
//-----------------------------------------------------------------------------
#ifndef Result_h
#define Result_h

#include <map>
#include <string>
#include <fstream>

#include "TH1F.h"
#include "TVectorD.h"
#include "TMatrixDSym.h"

#include "RooArgList.h"
#include "RooDataSet.h"
#include "RooMultiVarGaussian.h"

#include "Base.h"

class Result : public Base
{
public:
  Result(string,int);
  ~Result(){}

  string name(){return _name;}
  int numMeasurements(){return _nmeasurements;}
  int numParameters(){return modelParameters.size();}
  string parameterName(int i){return modelParameters[i];}
  int parameterIndexNumber(string nom){return Index[nom];}

  string measurementName(int);
  double measurementValue(int);

  void makeIndex();
  void printObservables(TH1F*,TH1F*);
  void useRealObservables();
  void useToyObservables();
  void useToyObservables(int);
  void addToIndex(map<string,int>*);

  virtual void writeLatex(fstream&);
  virtual void calculateObservables(double[]);
  virtual void generateObservables(int);
  virtual double chi2();

protected:
  TVectorD calculatedValues;
  TVectorD centralValues;
  TVectorD observedCentralValues;
  TVectorD statErrorValues;
  TVectorD systErrorValues;
  TMatrixDSym covarianceMatrix;
  TMatrixDSym covarianceMatrixInverted;
  map<string,TMatrixDSym> systematicsMatrix;
  RooArgList centralValueArgSet;
  RooArgList variablesArgSet;
  string _name;
  string _title;
  string _brief;
  int _nmeasurements;
  vector<string> modelParameters;
  map<string,int> Index;
  RooMultiVarGaussian* obsPdf;
  RooDataSet* obsData;
  bool usingToyObservables;
};

#endif