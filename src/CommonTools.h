//-----------------------------------------------------------------------------
// Declaration of the global namespace : CommonTools
//
// Purpose : Provies a few useful generalities: e.g. string manipulation...
//
// Author : Malcolm John, 2012-06-01
//-----------------------------------------------------------------------------
#ifndef COMMONTOOLS_H
#define COMMONTOOLS_H 1

// Include files
#include <map>
#include <vector>
#include <string>
#include "TH2.h"

/** @class CommonTools CommonTools.h
 *  
 *
 *  @author Malcolm John
 *  @date   2009-07-05
 */
namespace CommonTools
{
  bool exists(std::string);
  bool isADirectory(std::string);

  void defineColorMap();

  int getdir (std::string, std::vector<std::string> &);

  void ltrim(char*);
  void rtrim(char*);
  void  trim(char*);
  std::string& stringtrim(std::string&);
		
	void split(const std::string& ,std::vector<std::string>& ,const std::string&);

  std::string toPrecision(double, int);

  int decimalPlace(float,int);
  int signifFigures(float,int);

  std::string latex(std::string);
};
#endif // COMMONTOOLS_H
