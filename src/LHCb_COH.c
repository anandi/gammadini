//-----------------------------------------------------------------------------
// Implementation of class : LHCb_COH
//
// Responsibles : Malcolm John
//-----------------------------------------------------------------------------

#include "RooRealVar.h"
#include "TMath.h"
#include "TFile.h"
#include "LHCb_COH.h"

LHCb_COH::LHCb_COH(string label,float increasedLumiFactor)
  : Result(label, 1)// = Number of measurements
{//--------------------------------------------------------------------
  _title ="Coherence factors (LHCb)";
  _brief =string("This result contains update of the ration of BFs from Olli.");
  _brief+=string("See \\verb+http://arxiv.org/abs/1509.06628+");
  //--------------------------------------------------------------------
  modelParameters.push_back("x_D");
  modelParameters.push_back("y_D");
  modelParameters.push_back("rD_kskpi");
  modelParameters.push_back("deltaD_kskpi");
  modelParameters.push_back("R_kskpi");

  //variablesArgSet.add(* (new RooRealVar("R_kskpi_LHCb"   ,"R(KsK\\pi)_{LHCb}"   ,-1.0,2.0)));
  variablesArgSet.add(* (new RooRealVar("R_D_kskpi_LHCb" ,"R_D(KsK\\pi)_{LHCb}" ,-1.0,1.0)));

  makeIndex();//call after defining the variablesArgSet

  setup();

  covarianceMatrix += systematicsMatrix["ALL"];
  covarianceMatrix *= 1./increasedLumiFactor;
  covarianceMatrixInverted=covarianceMatrix;
  covarianceMatrixInverted.Invert();
  useRealObservables();
}

void LHCb_COH::setup()
{int i=0;int j=0;
  //observedCentralValues(Index["R_kskpi_LHCb"])          = 0.835;
  observedCentralValues(Index["R_D_kskpi_LHCb"])        = 0.370;
  //statErrorValues(Index["R_kskpi_LHCb"])                = sqrt(pow(0.003,2)+pow(0.011,2));
  statErrorValues(Index["R_D_kskpi_LHCb"])              = sqrt(pow(0.003,2)+pow(0.012,2));

  //i=Index["R_kskpi_LHCb"];        j=Index["R_kskpi_LHCb"];        covarianceMatrix(i,j) = statErrorValues(i)*statErrorValues(j) *    1   ;
  i=Index["R_D_kskpi_LHCb"];      j=Index["R_D_kskpi_LHCb"];      covarianceMatrix(i,j) = statErrorValues(i)*statErrorValues(j) *    1   ;
}

void LHCb_COH::calculateObservables(double param[])
{ // Calculate the expected measurement value based on a certain set of parameters.
  // This function is called at every step of the minimisation.

  double _x =param[Index["x_D"]];
  double _y =param[Index["y_D"]];
  double _rd=param[Index["rD_kskpi"]];
  double _dd=param[Index["deltaD_kskpi"]]/180.*TMath::Pi();
  double _R =param[Index["R_kskpi"]];
  //calculatedValues(Index["R_kskpi_LHCb"])        = _R;
  calculatedValues(Index["R_D_kskpi_LHCb"])      = ( pow(_rd,2) - _rd*_R*(_y*cos(_dd) - _x*sin(_dd)) ) / ( 1 - _rd*_R*(_y*cos(_dd) + _x*sin(_dd)) ) ;
}

void LHCb_COH::writeLatex(fstream& tex)
{
  Result::writeLatex(tex);
  tex<<"\\subsubsection*{Formulae for calculating observables from parameters}"<<endl;
  tex<<"\\begin{eqnarray*}"<<endl;
  tex<<variablesArgSet.find("R_D_kskpi_LHCb")->GetTitle()      <<"&=&"<<  "R_D(KsK\\pi)_{LHCb}"  <<"\\\\"<<endl;
  tex<<"\\end{eqnarray*}"<<endl;
}
