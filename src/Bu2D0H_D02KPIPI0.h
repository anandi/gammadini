//-----------------------------------------------------------------------------
// Declaration of class : Bu2D0H_D02KPIPI0
//-----------------------------------------------------------------------------
#ifndef Bu2D0H_D02KPIPI0_h
#define Bu2D0H_D02KPIPI0_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2D0H_D02KPIPI0 : public Result, ADSGLWBase
{
public :
  Bu2D0H_D02KPIPI0(string,float);
  ~Bu2D0H_D02KPIPI0(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private :
  bool dpionly;
  bool dkonly;
  bool nomix;
  
  void dkOnly();
  void dpiOnly();
  void both();
};

#endif