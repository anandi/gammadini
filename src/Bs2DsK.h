//-----------------------------------------------------------------------------
// Declaration of class : Bs2DsK
//-----------------------------------------------------------------------------
#ifndef Bs2DsK_h
#define Bs2DsK_h

#include "Result.h"

class Bs2DsK : public Result
{
public :
  Bs2DsK(string,float);
  ~Bs2DsK(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void final1ifb();
  double chi2();

  double currentPhis;
};

#endif