//-----------------------------------------------------------------------------
// Declaration of class : Pot
//
// Purpose : The Pot contains all information about all parameters AND the
//           contributing fit results. There should be only one pot per
//           instance of TMinuit. The Pot is called by fcn() when it
//           minimises the chi^2.
//           It inherits from TObject by necessity (to be passed by TMinuit).
//
// Author : Malcolm John, 2012-08-12
//-----------------------------------------------------------------------------
#ifndef Pot_h
#define Pot_h

#include <map>
#include <vector>

#include "TObject.h"
#include "TVectorD.h"
#include "TMatrixDSym.h"

#include "Result.h"
#include "Parameters.h"
#include "FitParameter.h"

using namespace std;

class Pot : public TObject {
public:
  Pot(Parameters*);
  ~Pot(){}

  void calculateObservables(double[]);
  double chi2(double[]);
  void generateSetOfToyObservables(int);
  double lastChi2(){return _lastchi2;}
  int numObservables();
  string observableName(int);
  double observableValue(int);
  int numParameters(){return parameters.size();}
  FitParameter* parameter(int);
  FitParameter* parameter(string);
  int parameterNumber(string nom){return parametersIndexNumber[nom];}
  void stir();
  void throwIn(string,float,int);
  void useToyObservables(int);
  void useToyObservables();
  void useRealObservables();
  void writeLatex();
  void plotGlobalFit(double);

private:
  Parameters* jobpar;
  vector<Result*> results;
  map<string,int> originalNumbers;
  vector<FitParameter*> parameterLibrary;
  map<string,FitParameter*> parameters;
  map<string,int> parametersIndexNumber;
  double _lastchi2;
};

#endif