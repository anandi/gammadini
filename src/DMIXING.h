//-----------------------------------------------------------------------------
// Declaration of class : DMIXING
//-----------------------------------------------------------------------------
#ifndef DMIXING_h
#define DMIXING_h

#include "Result.h"

class DMIXING : public Result
{
public :
  DMIXING(string,float);
  ~DMIXING(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);
  void hfag();
private:
  bool xandyonly;
};

#endif