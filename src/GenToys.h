//-----------------------------------------------------------------------------
// Declaration of class : GenToys
//
// Purpose : Generates pseudo-experiments based upon a given set of parameter
//           values. It asks the Pot to ask all the Results to provide
//           a set of psuedo-measurements generated from the central
//           values of the observables as calculated from the given parameter
//           set. The variation of the psuedo-measurements is provided
//           by the declared covaniance matrix of each Result.
//           Experimental likelihoods are not used for toy-generation as
//           this is considerably slower in generation step.
//
// Author : Malcolm John, 2012-08-22
//-----------------------------------------------------------------------------
#ifndef GenToys_h
#define GenToys_h

#include <map>

#include "TVectorF.h"
#include "TH2F.h"

#include "Method.h"
#include "FitParameter.h"

class GenToys : public Method {
  public :
  GenToys(Parameters*,vector<string>,map<string,int> );
  ~GenToys(){}
private:
  void generateToys(double,int=0);
  void generateParamterSets(int);
  void useParameterSet(int);

  FitParameter* poi;
  TVectorF* poiAtMin;
  TVectorF* deltaChi2;
  map<float,TH2F*> hello;

  map< int, vector<float> > store;
  bool writeBigNtuple;
};

#endif