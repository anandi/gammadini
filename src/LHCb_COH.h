//-----------------------------------------------------------------------------
// Declaration of class : LHCb_COH
//-----------------------------------------------------------------------------
#ifndef LHCb_COH_h
#define LHCb_COH_h

#include "Result.h"

class LHCb_COH : public Result
{
public :
  LHCb_COH(string,float=1);
  ~LHCb_COH(){}
  void setup();
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private :
};

#endif