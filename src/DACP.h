//-----------------------------------------------------------------------------
// Declaration of class : DACP
//-----------------------------------------------------------------------------
#ifndef DACP_h
#define DACP_h

#include "Result.h"

class DACP : public Result
{
public :
  DACP(string,float);
  ~DACP(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);
};

#endif