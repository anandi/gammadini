//-----------------------------------------------------------------------------
// Declaration of class : CLEO_COH
//-----------------------------------------------------------------------------
#ifndef CLEO_COH_h
#define CLEO_COH_h

#include "Result.h"
#include "TGraph2D.h"
#include "TH2F.h"

class CLEO_COH : public Result
{
public :
  CLEO_COH(string,float=1);
  ~CLEO_COH(){}
  void setup();
  void calculateObservables(double[]);
  void writeLatex(fstream&);
  double chi2();

private :
  TH2F*  cleoKPIPI0plot;
  TH2F*  cleoKPIPIPIplot;
  TH2F*  cleoKSKPIplot;

  bool includeKPIPI0;
  bool includeKPIPIPI;
  bool includeKSKPI;
};

#endif