//-----------------------------------------------------------------------------
// Declaration of class : Bu2D0Kstar_D02HH
//-----------------------------------------------------------------------------
#ifndef Bu2D0Kstar_D02HH_h
#define Bu2D0Kstar_D02HH_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2D0Kstar_D02HH : public Result, ADSGLWBase
{
public :
  Bu2D0Kstar_D02HH(string,float);
  ~Bu2D0Kstar_D02HH(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void results();
  bool nomix;
};

#endif
