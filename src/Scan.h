//-----------------------------------------------------------------------------
// Declaration of class : Scan
//
// Purpose : Performs a Delta(chi2) scan of a parameter of interest.
//           A 2D scan function is also enabled which converts the
//           Delta(chi2) values into probabilities assuming the
//           Delta(chi2) distribution is that of a Gaussian.
//
// Author : Malcolm John, 2012-08-10
//-----------------------------------------------------------------------------
#ifndef Scan_h
#define Scan_h
#include <map>

#include "Method.h"

class Scan : public Method
{
public :
  Scan(Parameters*,vector<string>,map<string,int> );
  ~Scan(){}

private:
  void oneDscan(string);
  void twoDscan(string,string);

  map< FitParameter*,vector<double> > partrend;
  map< FitParameter*,vector<double> > errtrend;
};

#endif