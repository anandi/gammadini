//-----------------------------------------------------------------------------
// Declaration of class : CARTESIAN
//-----------------------------------------------------------------------------
#ifndef CARTESIAN_h
#define CARTESIAN_h

#include "Result.h"
#include "TH2F.h"

class CARTESIAN : public Result
{
public :
  CARTESIAN(string,float=1);
  ~CARTESIAN(){}
  void setup();
  void calculateObservables(double[]);
  void writeLatex(fstream&);
  double chi2();

private :
  TH2F*  plotBu_dpi_plus;
  TH2F*  plotBu_dpi_minus;
  TH2F*  plotBu_dk_plus;
  TH2F*  plotBu_dk_minus;
  TH2F*  plotBd_dkpi_plus;
  TH2F*  plotBd_dkpi_minus;

  bool includeBd_dkpi;
  bool includeBu_dpi;
  bool includeBu_dk;
  bool dpionly;
  bool dkonly;
};

#endif