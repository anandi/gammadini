//-----------------------------------------------------------------------------
// Declaration of class : ADSGLWBase
//
// Purpose : Base class containing master formulae of ADSGLW formulae
//
// Author : Malcolm John, 2015-07-30
//-----------------------------------------------------------------------------
#ifndef ADSGLWBase_h
#define ADSGLWBase_h

#include <cmath>

class ADSGLWBase
{
public:
  ADSGLWBase();
  ~ADSGLWBase(){}

  void debug();
protected:
  bool cart;

  double g;
  double db;
  double dd;
  double rb;
  double rd;
  double rb2;
  double rd2;
  double kd;
  double kb;
  double x;
  double y;
  double Mxy;
  double xp;
  double yp;
  double xm;
  double ym;
  
  double G_b2DX_CPP();
  double G_bbar2DX_CPP();
  double G_b2DX_CPM();
  double G_bbar2DX_CPM();
  double G_b2DX_ADS();
  double G_bbar2DX_ADS();
  double G_b2DX_FAV();
  double G_bbar2DX_FAV();
};

inline double ADSGLWBase::G_b2DX_CPP()
{
  if(cart){
    rb2=(pow(xm,2)+pow(ym,2));
    return 1+rb2+2*kb*kd*xm;
  }
  return 1+rb2+2*kb*kd*rb*cos(db-g);
}

inline double ADSGLWBase::G_bbar2DX_CPP()
{
  if(cart){
    rb2=(pow(xp,2)+pow(yp,2));
    return 1+rb2+2*kb*kd*xp;
  }
  return 1+rb2+2*kb*kd*rb*cos(db+g);
}

inline double ADSGLWBase::G_b2DX_CPM()
{
  if(cart){
    rb2=(pow(xm,2)+pow(ym,2));
    return 1+rb2-2*kb*kd*xm;
  }
  return 1+rb2-2*kb*kd*rb*cos(db-g);
}

inline double ADSGLWBase::G_bbar2DX_CPM()
{
  if(cart){
    rb2=(pow(xp,2)+pow(yp,2));
    return 1+rb2-2*kb*kd*xp;
  }
  return 1+rb2-2*kb*kd*rb*cos(db+g);
}

inline double ADSGLWBase::G_b2DX_ADS()
{
  if(cart){
    rb2=(pow(xm,2)+pow(ym,2));
    return rd2 + rb2 + 2*rd*kb*kd*(xm*cos(dd)-ym*sin(dd))
    + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*kb*xm)*Mxy*y
    - 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*kb*ym)*Mxy*x;
  }
  return rd2 + rb2 + 2*rd*rb*kb*kd*cos(db+dd-g)
  + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*rb*kb*cos(db-g))*Mxy*y
  - 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*rb*kb*sin(db-g))*Mxy*x;
}

inline double ADSGLWBase::G_bbar2DX_ADS()
{
  if(cart){
    rb2=(pow(xp,2)+pow(yp,2));
    return rd2 + rb2 + 2*rd*kb*kd*(xp*cos(dd)-yp*sin(dd))
    + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*kb*xp)*Mxy*y
    - 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*kb*yp)*Mxy*x;
  }
  return rd2 + rb2 + 2*rd*rb*kb*kd*cos(db+dd+g)
  + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*rb*kb*cos(db+g))*Mxy*y
  - 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*rb*kb*sin(db+g))*Mxy*x;
}

inline double ADSGLWBase::G_b2DX_FAV()
{
  if(cart){
    rb2=(pow(xm,2)+pow(ym,2));
    return 1 + rd2*rb2 + 2*rd*kb*kd*(xm*cos(dd)+ym*sin(dd))
    + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*kb*xm)*Mxy*y
    + 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*kb*ym)*Mxy*x;
  }
  return 1 + rd2*rb2 + 2*rd*rb*kb*kd*cos(db-dd-g)
  + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*rb*kb*cos(db-g))*Mxy*y
  + 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*rb*kb*sin(db-g))*Mxy*x;
}

inline double ADSGLWBase::G_bbar2DX_FAV()
{
  if(cart){
    rb2=(pow(xp,2)+pow(yp,2));
    return 1 + rd2*rb2 + 2*rd*kb*kd*(xp*cos(dd)+yp*sin(dd))
    + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*kb*xp)*Mxy*y
    + 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*kb*yp)*Mxy*x;
  }
  return 1 + rd2*rb2 + 2*rd*rb*kb*kd*cos(db-dd+g)
  + 2*((1+rb2)*rd*kd*cos(dd)+(1+rd2)*rb*kb*cos(db+g))*Mxy*y
  + 2*((1-rb2)*rd*kd*sin(dd)-(1-rd2)*rb*kb*sin(db+g))*Mxy*x;
}

#endif