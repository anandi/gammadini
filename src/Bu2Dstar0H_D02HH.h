//-----------------------------------------------------------------------------
// Declaration of class : Bu2Dstar0H_D02HH
//-----------------------------------------------------------------------------
#ifndef Bu2Dstar0H_D02HH_h
#define Bu2Dstar0H_D02HH_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2Dstar0H_D02HH : public Result, ADSGLWBase
{
public :
  Bu2Dstar0H_D02HH(string,float);
  ~Bu2Dstar0H_D02HH(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void onlyCP();
  void bothCPandADS();
  bool cponly;
  bool nomix;
};

#endif