//-----------------------------------------------------------------------------
// Declaration of class : Bd2D0KPI_D02HH
//-----------------------------------------------------------------------------
#ifndef Bd2D0KPI_D02HH_h
#define Bd2D0KPI_D02HH_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bd2D0KPI_D02HH : public Result, ADSGLWBase
{
public :
  Bd2D0KPI_D02HH(string,float);
  ~Bd2D0KPI_D02HH(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void summer2014();
  void winter2016();
  void winter2016WarwickOnly();
  bool warwickOnly;
  bool nomix;
};

#endif