//-----------------------------------------------------------------------------
// Declaration of class : Coverage
//
// Purpose : Generates sets of pseudo-parameters based upon parameters'
//           given initial value and declared uncertainty. Observable
//           sets are constructed and minimised using the real-data
//           covariance matrices. Results are written to file for
//           re-reading and plotting.
//
// Author : Malcolm John, 2015-10-24
//-----------------------------------------------------------------------------
#ifndef Coverage_h
#define Coverage_h

#include <map>

#include "TVectorF.h"
#include "TH2F.h"

#include "Method.h"
#include "FitParameter.h"

class Coverage : public Method {
  public :
  Coverage(Parameters*,vector<string>,map<string,int> );
  ~Coverage(){}

private:
  void generateToys(int=0);
  void generateParamterSets(int);

  FitParameter* poi;
  TH2F* chi2H;
  TH1F* chi1H;
  TH2F* monH;
  TH1F* covH;

  map< int, vector<float> > store;
};

#endif