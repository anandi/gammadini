//-----------------------------------------------------------------------------
// Declaration of class : CLEO_FCP
//-----------------------------------------------------------------------------
#ifndef CLEO_FCP_h
#define CLEO_FCP_h

#include "Result.h"

class CLEO_FCP : public Result
{
public :
  CLEO_FCP(string,float=1);
  ~CLEO_FCP(){}
  void setup();
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private :
  bool includeKPIPI0;
  bool includeKPIPIPI;
};

#endif