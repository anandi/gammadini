//-----------------------------------------------------------------------------
// Declaration of class : Bd2D0KPI_D02KPIPIPI
//-----------------------------------------------------------------------------
#ifndef Bu2D0H_D02KPIPIPI_h
#define Bu2D0H_D02KPIPIPI_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2D0H_D02KPIPIPI : public Result, ADSGLWBase
{
public :
  Bu2D0H_D02KPIPIPI(string,float);
  ~Bu2D0H_D02KPIPIPI(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void both();
  void final2011();
  void final2015();

  void dpiOnly();
  void dpiOnly2011();
  void dpiOnly2015();
  void dkOnly();
  void dkOnly2011();
  void dkOnly2015();

  bool dpionly;
  bool dkonly;
  bool nomix;
};

#endif