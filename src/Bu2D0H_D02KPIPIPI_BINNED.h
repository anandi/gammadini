//-----------------------------------------------------------------------------
// Declaration of class : Bd2D0KPI_D02KPIPIPI
//-----------------------------------------------------------------------------
#ifndef Bu2D0H_D02KPIPIPI_BINNED_h
#define Bu2D0H_D02KPIPIPI_BINNED_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2D0H_D02KPIPIPI_BINNED : public Result, ADSGLWBase
{
public :
  Bu2D0H_D02KPIPIPI_BINNED(string,float);
  ~Bu2D0H_D02KPIPIPI_BINNED(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void both();
  void final2011();
  void final2015();

  void dpiOnly();
  void dpiOnly2011();
  void dpiOnly2015();
  void dkOnly();
  void dkOnly2011();
  void dkOnly2015();

  bool dpionly;
  bool dkonly;
  bool nomix;

  vector<float> deltad_bin;
  vector<float> rd_bin;
  vector<float> kappa_bin;
  vector<float> Rm_DK;
  vector<float> Rp_DK;
  vector<float> Rm_Dpi;
  vector<float> Rp_Dpi;
  vector<float> Rm_DK_err;
  vector<float> Rp_DK_err;
  vector<float> Rm_Dpi_err;
  vector<float> Rp_Dpi_err;
  int nbins;

  void defineDalitzBins();
};

#endif