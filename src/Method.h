//-----------------------------------------------------------------------------
// Declaration of class : Method
//
// Purpose : This is a base class for any given statistical method.
//           It runs the nominal fit that finds the global minimum.
//
// Author : Malcolm John, 2012-06-01
//-----------------------------------------------------------------------------
#ifndef Method_h
#define Method_h

#include "TFitter.h"

#include "Base.h"
#include "Parameters.h"
#include "FitParameter.h"
#include "Pot.h"

class Method : public Base {
  public :
  Method(Parameters*,vector<string>,map<string,int>);
  ~Method(){}

  void initialiseParameters(FitParameter* p=0,double d=0);
  int initiateMinimiser();
  int primaryMinimisation();
  int minimise(bool=false);

  int findMinimum(int,string,bool=false);

protected:
  Parameters* jobpar;
  FitParameter* poi;
  FitParameter* sec;
  TFitter* minimiser;
  int nObs;
  int nParams;
  Pot* pot;
  double chi2AtGlobalMin;
  map<FitParameter*,double> bestVal;
  map<FitParameter*,double> defaultVal;
  int reminimiseCount;
};

#endif