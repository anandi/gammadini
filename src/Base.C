//-----------------------------------------------------------------------------
// Implementation of class : Base
//
// Author : Malcolm John, 2012-05-31
//-----------------------------------------------------------------------------

#include <math.h>

#include "TAxis.h"

#include "Base.h"
#include "CommonTools.h"

Base::Base(string nn)
  : name(nn)
	, Gen("Gen")
  , null("null")
  , underscore("_")
  , colon(":")
  , slash("/")
  , pi(3.141592653589793)
  , twopi(6.283185307179586)
  , toygen("toygen")
  , scan("scan")
  , debug("debug")
  , coverage("coverage")
  , nothing(-9999)
  , initialvalues("initialvalues")
  , currentvalues("currentvalues")
  , globalminvalues("globalminvalues")
{
  lhcblabel = new TPaveLabel(0.7,0.80,0.9,0.90,"LHCb","NDC");
  lhcblabel->SetBorderSize(0);
  lhcblabel->SetFillStyle(0);
  lhcblabel->SetTextFont(62);
  lhcblabel->SetTextAlign(21);
  
  preliminarylabel = new TPaveLabel(0.7,0.763,0.9,0.795,"Preliminary","NDC");
  preliminarylabel->SetBorderSize(0);
  preliminarylabel->SetFillStyle(0);
  preliminarylabel->SetTextFont(62);
  preliminarylabel->SetTextAlign(23);
}

pair<double,double> Base::weightedAverage(double x, double sx, double y,double sy)
{
  float wx = pow(sx,-2);
  float wy = pow(sy,-2);
  float w  = wx+wy;
  float ave = (wx*x+wy*y)/w;
  float err = 1./sqrt(w);
  return make_pair<double,double>(ave,err);
}


pair<double,double> Base::errorPropDiv(double x, double sx, double y,double sy)
{
  float rx = pow(sx/x,2);
  float ry = pow(sy/y,2);
  float val = x/y;
  float err = val*pow(rx+ry,0.5);
  return make_pair<double,double>(val,err);
}

pair<double,double> Base::errorPropMult(double x, double sx, double y,double sy)
{
  float rx = pow(sx/x,2);
  float ry = pow(sy/y,2);
  float val = x*y;
  float err = val*pow(rx+ry,0.5);
  return make_pair<double,double>(val,err);
}

pair<double,double> Base::errorPropAdd(double x, double sx, double y,double sy)
{
  float rx = pow(sx,2);
  float ry = pow(sy,2);
  float val =x+y;
  float err =pow(rx+ry,0.5);
  return make_pair<double,double>(val,err);
}

pair<double,double> Base::errorPropSub(double x, double sx, double y,double sy)
{
  float rx = pow(sx,2);
  float ry = pow(sy,2);
  float val =x-y;
  float err =pow(rx+ry,0.5);
  return make_pair<double,double>(val,err);
}


int Base::interpolationCoeffs(TH2* h, double x, double y)
{
  // Bicubic interpolation. Code lifted from http://www.paulinternet.nl

  int i1 = h->GetXaxis()->FindBin(x);
  int j1 = h->GetYaxis()->FindBin(y);

  if(x<h->GetXaxis()->GetBinCenter(i1)) i1-- ;
  if(y<h->GetYaxis()->GetBinCenter(j1)) j1-- ;

  int i0=i1-1; int j0=j1-1;
  //cout<<"Interpolation: x = "<< x <<", y = "<<y<<"    i0 = "<<i0<< "[1:"<<h->GetXaxis()->GetNbins()<<"]"<<"    j0 = "<<j0<< "[1:"<<h->GetYaxis()->GetNbins()<<"]"<<endl;

  if((i0<1) || (j0<1) || (i0>h->GetXaxis()->GetNbins()-3) || (j0>h->GetYaxis()->GetNbins()-3)){
    //cout<<"Interpolation is too near edge"<<endl;
    return 1;
  }else if( 0==h->GetBinContent(i0  ,j0) || 0==h->GetBinContent(i0  ,j0+3) ||
            0==h->GetBinContent(i0+3,j0) || 0==h->GetBinContent(i0+3,j0+3) ){
    //cout<<"Interpolation falls into undefinded regions"<<endl;
    return 1;
  }


  if(i1==i1_cached && j1==j1_cached) return 0;
  i1_cached=i1;
  j1_cached=j1;

  
  double p[4][4];
  for(unsigned int i=0;i<4;i++){
    for(unsigned int j=0;j<4;j++){
      p[i][j]=h->GetBinContent(i0+i,j0+j);
    }
  }

  a00 =     p[1][1];
  a01 = -.5*p[1][0] + 0.5*p[1][2];
  a02 =     p[1][0] - 2.5*p[1][1] + 2*  p[1][2] - .5*p[1][3];
  a03 = -.5*p[1][0] + 1.5*p[1][1] - 1.5*p[1][2] + .5*p[1][3];
  a10 = -.5*p[0][1] + 0.5*p[2][1];
  a11 = .25*p[0][0] - .25*p[0][2] - .25*p[2][0] + .25*p[2][2];
  a12 = -.5*p[0][0] +1.25*p[0][1] - p[0][2] + .25*p[0][3] + .5*p[2][0] - 1.25*p[2][1] + p[2][2] - .25*p[2][3];
  a13 = .25*p[0][0] -0.75*p[0][1] + .75*p[0][2] - .25*p[0][3] - .25*p[2][0] + .75*p[2][1] - .75*p[2][2] + .25*p[2][3];
  a20 =     p[0][1] - 2.5*p[1][1] + 2*p[2][1] - .5*p[3][1];
  a21 = -.5*p[0][0] + 0.5*p[0][2] + 1.25*p[1][0] - 1.25*p[1][2] - p[2][0] + p[2][2] + .25*p[3][0] - .25*p[3][2];
  a22 =     p[0][0] - 2.5*p[0][1] + 2*p[0][2] - .5*p[0][3] - 2.5*p[1][0] + 6.25*p[1][1] - 5*p[1][2] + 1.25*p[1][3] + 2*p[2][0] - 5*p[2][1] + 4*p[2][2] - p[2][3] - .5*p[3][0] + 1.25*p[3][1] - p[3][2] + .25*p[3][3];
  a23 = -.5*p[0][0] + 1.5*p[0][1] - 1.5*p[0][2] + .5*p[0][3] + 1.25*p[1][0] - 3.75*p[1][1] + 3.75*p[1][2] - 1.25*p[1][3] - p[2][0] + 3*p[2][1] - 3*p[2][2] + p[2][3] + .25*p[3][0] - .75*p[3][1] + .75*p[3][2] - .25*p[3][3];
  a30 = -.5*p[0][1] + 1.5*p[1][1] - 1.5*p[2][1] + .5*p[3][1];
  a31 = .25*p[0][0] -0.25*p[0][2] - .75*p[1][0] + .75*p[1][2] + .75*p[2][0] - .75*p[2][2] - .25*p[3][0] + .25*p[3][2];
  a32 = -.5*p[0][0] +1.25*p[0][1] - p[0][2] + .25*p[0][3] + 1.5*p[1][0] - 3.75*p[1][1] + 3*p[1][2] - .75*p[1][3] - 1.5*p[2][0] + 3.75*p[2][1] - 3*p[2][2] + .75*p[2][3] + .5*p[3][0] - 1.25*p[3][1] + p[3][2] - .25*p[3][3];
  a33 = .25*p[0][0] -0.75*p[0][1] + .75*p[0][2] - .25*p[0][3] - .75*p[1][0] + 2.25*p[1][1] - 2.25*p[1][2] + .75*p[1][3] + .75*p[2][0] - 2.25*p[2][1] + 2.25*p[2][2] - .75*p[2][3] - .25*p[3][0] + .75*p[3][1] - .75*p[3][2] + .25*p[3][3];

  return 0;
}


double Base::getInterpolatedValue (TH2* h, double x, double y) {

  x = (x-h->GetXaxis()->GetBinCenter(i1_cached))/h->GetXaxis()->GetBinWidth(i1_cached);
  y = (y-h->GetYaxis()->GetBinCenter(j1_cached))/h->GetYaxis()->GetBinWidth(j1_cached);

  double x2 = x * x;
  double x3 = x * x * x;
  double y2 = y * y;
  double y3 = y * y * y;

  return  (a00 + a01 * y + a02 * y2 + a03 * y3) * 1  +
  (a10 + a11 * y + a12 * y2 + a13 * y3) * x  +
  (a20 + a21 * y + a22 * y2 + a23 * y3) * x2 +
  (a30 + a31 * y + a32 * y2 + a33 * y3) * x3 ;
}