//-----------------------------------------------------------------------------
// Declaration of class : Bu2D0H_D02KSHH
//-----------------------------------------------------------------------------
#ifndef Bu2D0H_D02KSHH_h
#define Bu2D0H_D02KSHH_h

#include "Result.h"

class Bu2D0H_D02KSHH : public Result
{
public :
  Bu2D0H_D02KSHH(string,float);
  ~Bu2D0H_D02KSHH(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void modelIndepedent();
  bool dpionly;
  bool cart;
};

#endif