//-----------------------------------------------------------------------------
// Declaration of class : Parameters
//
// Purpose : Interprets the command line arguments.
//           A pointer to the class is provided by Main.C so commandline
//           arguments can be available anywhere in the programme.
//
// Author : Malcolm John, 2012-05-31
//-----------------------------------------------------------------------------
#ifndef PARAMETERS_H
#define PARAMETERS_H 1

// Include files
#include<iostream>
#include<string>
#include<vector>
#include<map>

#include "Base.h"

class Parameters : public Base {
 public: 
  /// Standard constructor
  Parameters( ); 
  int readCommandLine(unsigned int,char**);
  bool exists(string);
  void defineTypeColors();
  void banner();
  void help();

  virtual ~Parameters(){}

  bool batch;
  string location;
  string toyfile;
  bool db;
  int ntoys;
  string task;
  string poi;
  string sec;
  float increase;
  vector<unsigned int> remove;
  int granularity;
  float setVal;
  bool extended;
  bool quick;
  bool check;
  bool dkonly;
  bool dpionly;
  bool noDmixing;
  bool cartesian;
  bool dstarcponly;
  bool coverageTest;
  string subtitle;
  vector<string> mode;
  bool collectionMode;
};
#endif // PARAMETERS_H
