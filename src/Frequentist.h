//-----------------------------------------------------------------------------
// Declaration of class : Frequentist
//
// Purpose : Top-level class. Identifies what type of processing is needed.
//
// Author : Malcolm John, 2012-05-31
//-----------------------------------------------------------------------------
#ifndef Frequentist_h
#define Frequentist_h

#include <map>

#include "Base.h"

class Parameters;

class Frequentist : public Base {
 public :
  Frequentist(Parameters*);
  ~Frequentist(){}
 private :
  vector<string> modes;
  map<string,int> originalNumbers;
};

#endif
