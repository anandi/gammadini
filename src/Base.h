//-----------------------------------------------------------------------------
// Declaration of class : Base
//
// Purpose : Rather under-used base class from which most other classes inherit
//
// Author : Malcolm John, 2012-05-31
//-----------------------------------------------------------------------------
#ifndef BASE_H
#define BASE_H 1

#include<utility>
#include<iostream>
#include<string>
#include<vector>
#include<map>

#include "TPaveLabel.h"
#include "TH2.h"

using namespace std;

class Base {
public: 
  /// Standard constructor
  Base(string n="undefined"); 
  virtual ~Base(){}

  pair<double,double> weightedAverage(double,double,double,double);
  pair<double,double> errorPropDiv(double,double,double,double);
  pair<double,double> errorPropMult(double,double,double,double);
  pair<double,double> errorPropAdd(double,double,double,double);
  pair<double,double> errorPropSub(double,double,double,double);

  int     interpolationCoeffs(TH2*,double,double);
  double getInterpolatedValue(TH2*,double,double);

protected:
  string name;
  string Gen;
  string null;
  string underscore;
  string colon;
  string slash;
  double pi;
  double twopi;
  TPaveLabel* lhcblabel;
  TPaveLabel* preliminarylabel;
  string toygen;
  string scan;
  string debug;
  string coverage;
  float nothing;
  string initialvalues;
  string currentvalues;
  string globalminvalues;

  private:

  double a00, a01, a02, a03;
  double a10, a11, a12, a13;
  double a20, a21, a22, a23;
  double a30, a31, a32, a33;

  int i1_cached; int j1_cached;
};



#endif // BASE_H
