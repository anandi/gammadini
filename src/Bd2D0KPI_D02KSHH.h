//-----------------------------------------------------------------------------
// Declaration of class : Bd2D0KPI_D02KSHH
//-----------------------------------------------------------------------------
#ifndef Bd2D0KPI_D02KSHH_h
#define Bd2D0KPI_D02KSHH_h

#include "Result.h"

class Bd2D0KPI_D02KSHH : public Result
{
public :
  Bd2D0KPI_D02KSHH(string,float);
  ~Bd2D0KPI_D02KSHH(){}
  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void winter2016();
  bool cart;
  bool modindep;
};

#endif