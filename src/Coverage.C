//-----------------------------------------------------------------------------
// Implementation of class : Coverage
//
// Author : Malcolm John, 2015-10-24
//-----------------------------------------------------------------------------

#include <vector>
#include <iostream>
#include <map>
#include <cmath>
#include <time.h>

#include "TROOT.h"
#include "TFile.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TMinuit.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TLine.h"
#include "TTree.h"
#include "TRandom3.h"

#include "Parameters.h"
#include "CommonTools.h"
#include "Coverage.h"

Coverage::Coverage(Parameters* p,vector<string> modes,map<string,int> originalNumbers)
: Method(p, modes,originalNumbers)
{
  if( initiateMinimiser() ){ p->batch=true; return; }

  poi = pot->parameter(jobpar->poi);
  covH = new TH1F("covH" ,"fitted",20,poi->loPlotLim(),poi->hiPlotLim());
  monH = new TH2F("monH" ,"gen vs. fitted",20,poi->loPlotLim(),poi->hiPlotLim(),20,poi->loPlotLim(),poi->hiPlotLim());
  chi2H= new TH2F("chi2H","gen vs. fitted chi2",20,poi->loPlotLim(),poi->hiPlotLim(),20,poi->loPlotLim(),poi->hiPlotLim());
  chi1H= new TH1F("chi1H","chi2",20,0.,3.);


  generateParamterSets(jobpar->ntoys);
  for(int j=0;j<jobpar->ntoys;j++){
    if(0==j%100)cout<<"\r"<<j<<"..."<<flush;
    generateToys(j);
  }cout<<endl;


  TCanvas* covCan = new TCanvas("covCan","Coverage",1000,1000);
  covCan->Divide(2,2);
  covCan->cd(1);
  monH->Draw("COL");
  monH->SetStats(0);
  monH->GetXaxis()->SetTitle( (poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  monH->GetXaxis()->SetTitleOffset(1.1);
  covCan->cd(1)->SetTopMargin(0.07);
  covCan->cd(1)->SetLeftMargin(0.15);
  covCan->cd(1)->SetBottomMargin(0.13);
  covCan->cd(1)->SetRightMargin(0.15);
  covCan->cd(1)->SetFrameLineWidth(3);
  monH->SetTitle("");
  monH->GetXaxis()->SetTitle( (string("generated ")+poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  monH->GetYaxis()->SetTitle( (string("fitted ")   +poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  monH->GetXaxis()->SetTitleOffset(1.1);
  monH->GetYaxis()->SetTitleOffset(1.4);
  monH->GetXaxis()->SetLabelFont(132);
  monH->GetYaxis()->SetLabelFont(132);
  monH->GetXaxis()->SetTitleFont(132);
  monH->GetYaxis()->SetTitleFont(132);
  monH->GetXaxis()->SetLabelSize(0.05);
  monH->GetXaxis()->SetTitleSize(0.05);
  monH->GetYaxis()->SetLabelSize(0.05);
  monH->GetYaxis()->SetTitleSize(0.05);
  monH->GetXaxis()->SetNdivisions(5);
  monH->GetYaxis()->SetNdivisions(5);

  covCan->cd(2);
  covCan->cd(2)->SetTopMargin(0.07);
  covCan->cd(2)->SetLeftMargin(0.15);
  covCan->cd(2)->SetBottomMargin(0.13);
  covCan->cd(2)->SetRightMargin(0.15);
  covCan->cd(2)->SetFrameLineWidth(3);
  covH->Draw();
  covH->SetStats(0);
  covH->SetTitle("");
  covH->GetXaxis()->SetTitle( (string("fitted ")   +poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  covH->GetXaxis()->SetTitleOffset(1.1);
  covH->GetYaxis()->SetTitleOffset(1.4);
  covH->GetXaxis()->SetLabelFont(132);
  covH->GetYaxis()->SetLabelFont(132);
  covH->GetXaxis()->SetTitleFont(132);
  covH->GetYaxis()->SetTitleFont(132);
  covH->GetXaxis()->SetLabelSize(0.05);
  covH->GetXaxis()->SetTitleSize(0.05);
  covH->GetYaxis()->SetLabelSize(0.05);
  covH->GetYaxis()->SetTitleSize(0.05);
  covH->GetXaxis()->SetNdivisions(5);
  covH->GetYaxis()->SetNdivisions(5);
  covH->SetMinimum(0.0000005);

  covCan->cd(3);
  chi2H->Divide(monH);
  chi2H->Draw("COLZ");
  chi2H->SetStats(0);
  chi2H->GetXaxis()->SetTitle( (poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  chi2H->GetXaxis()->SetTitleOffset(1.1);
  covCan->cd(3)->SetTopMargin(0.07);
  covCan->cd(3)->SetLeftMargin(0.15);
  covCan->cd(3)->SetBottomMargin(0.13);
  covCan->cd(3)->SetRightMargin(0.15);
  covCan->cd(3)->SetFrameLineWidth(3);
  chi2H->SetTitle("");
  chi2H->GetXaxis()->SetTitle( (string("generated ")+poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  chi2H->GetYaxis()->SetTitle( (string("fitted ")   +poi->title()+(0==poi->unit().size()?"":" ["+poi->unit()+"]")).c_str() );
  chi2H->GetXaxis()->SetTitleOffset(1.1);
  chi2H->GetYaxis()->SetTitleOffset(1.4);
  chi2H->GetXaxis()->SetLabelFont(132);
  chi2H->GetYaxis()->SetLabelFont(132);
  chi2H->GetXaxis()->SetTitleFont(132);
  chi2H->GetYaxis()->SetTitleFont(132);
  chi2H->GetXaxis()->SetLabelSize(0.05);
  chi2H->GetXaxis()->SetTitleSize(0.05);
  chi2H->GetYaxis()->SetLabelSize(0.05);
  chi2H->GetYaxis()->SetTitleSize(0.05);
  chi2H->GetXaxis()->SetNdivisions(5);
  chi2H->GetYaxis()->SetNdivisions(5);

  covCan->cd(4);
  covCan->cd(4)->SetTopMargin(0.07);
  covCan->cd(4)->SetLeftMargin(0.15);
  covCan->cd(4)->SetBottomMargin(0.13);
  covCan->cd(4)->SetRightMargin(0.15);
  covCan->cd(4)->SetFrameLineWidth(3);
  chi1H->Draw();
  chi1H->SetStats(0);
  chi1H->SetTitle("");
  chi1H->GetXaxis()->SetTitle( "#chi^{2}" );
  chi1H->GetXaxis()->SetTitleOffset(1.1);
  chi1H->GetYaxis()->SetTitleOffset(1.4);
  chi1H->GetXaxis()->SetLabelFont(132);
  chi1H->GetYaxis()->SetLabelFont(132);
  chi1H->GetXaxis()->SetTitleFont(132);
  chi1H->GetYaxis()->SetTitleFont(132);
  chi1H->GetXaxis()->SetLabelSize(0.05);
  chi1H->GetXaxis()->SetTitleSize(0.05);
  covH->GetYaxis()->SetLabelSize(0.05);
  chi1H->GetYaxis()->SetTitleSize(0.05);
  chi1H->GetXaxis()->SetNdivisions(5);
  chi1H->GetYaxis()->SetNdivisions(5);
  chi1H->SetMinimum(0.0000005);
}


void Coverage::generateParamterSets(int nSets)
{
  TRandom* rand = new TRandom3(time(NULL));

  for(int i=0;i<nParams;i++){
    FitParameter* par = pot->parameter(i);
    store.insert(make_pair(i,vector<float>(nSets)));

    for(int j=0;j<nSets;j++){
      float val=par->initialVal();
      if(poi==par) val = rand->Uniform(par->loPlotLim(),par->hiPlotLim());
      store[i][j]=val;
    }
  }
}


void Coverage::generateToys(int j)
{
  float poiGenerated=0;
  double param[nParams];
  for(int i=0;i<nParams;i++){
    param[i]=store[i][j];
    FitParameter* par = pot->parameter(i);
    if(poi==par) poiGenerated=param[i];
  }

  pot->calculateObservables(param);
  pot->generateSetOfToyObservables(1);
  pot->useToyObservables(0);
  findMinimum(2,initialvalues);


  Double_t fmin; Double_t fedm; Double_t errdef; Int_t npari; Int_t nparx; Int_t istat;
  minimiser->GetMinuit()->mnstat(fmin, fedm, errdef, npari, nparx, istat);
  if( fedm>1 or istat!=3 ) return;

  chi1H->Fill(pot->lastChi2()/(pot->numObservables()-pot->numParameters()));
  chi2H->Fill(poiGenerated,poi->value(),pot->lastChi2()/(pot->numObservables()-pot->numParameters()));
  monH->Fill(poiGenerated,poi->value());
  covH->Fill(poi->value());
}



