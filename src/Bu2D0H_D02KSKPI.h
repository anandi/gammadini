//-----------------------------------------------------------------------------
// Declaration of class : Bu2D0H_D02KSKPI
//-----------------------------------------------------------------------------
#ifndef Bu2D0H_D02KSKPI_h
#define Bu2D0H_D02KSKPI_h

#include "Result.h"
#include "ADSGLWBase.h"

class Bu2D0H_D02KSKPI : public Result, ADSGLWBase
{
public :
  Bu2D0H_D02KSKPI(string,float);
  ~Bu2D0H_D02KSKPI(){}

  void calculateObservables(double[]);
  void writeLatex(fstream&);

private:
  void modind3ifb();
  bool dpionly;
  bool dkonly;
  bool both;
  bool nomix;
};

#endif

